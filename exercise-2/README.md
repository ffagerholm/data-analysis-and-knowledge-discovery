
# Exercise work 2

Report is in the file `analysis_report_2.html` in the format of a html file. 
It includes all the code, plots, tables etc.


## To run notebook

### GitHub  
The notebook can be opened directly in GitHub by opening the file.  

### Run with jupyter notebooks  
Code can be run as a ipython notebook.  
Requires that `python` and `pip` are installed.  
Additional requirements are in the file `requirements.txt`.  
To install requirements needed to run the notebook, run:  
    `pip install -r requirements.txt`  
To start notebook:   
    `ipython notebook analysis_report_2.ipynb`  
