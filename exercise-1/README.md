
# Exercise work 1

Report is in the file `analysis_report.html` in the format of a html file. 
It includes all the code, plots, tables etc.


## To run notebook

Code can be run as a ipython notebook.  

### In github
The notebook can be opened directly in GitHub by opening the file. 

### Run with jupyter notebooks
Requires that `python` and `pip` are installed.  
Additional requirements are in the file `requirements.txt`.  
To install requirements needed to run the notebook, run:  
`pip install -r requirements.txt`  
To start notebook:    
`ipython notebook analysis_report.ipynb`  
